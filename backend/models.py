from django.db import models

class Scoreboard(models.Model):
    name = models.CharField(max_length=10)
    point = models.CharField(max_length=10)

    def __str__(self):
        return "{} {}".format(self.name,self.point)