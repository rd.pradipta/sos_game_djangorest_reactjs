from rest_framework import serializers
from backend.models import Scoreboard

class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scoreboard
        fields = '__all__'