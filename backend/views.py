from backend.models import Scoreboard
from backend.serializers import DataSerializer
from rest_framework import generics

class ScoreAdder(generics.ListCreateAPIView):
    queryset = Scoreboard.objects.all()
    serializer_class = DataSerializer