## SOS Game - Another Version

This is a (very) simple Tic Tac Toe modified game called SOS. This game inspired from Hago (mobile multiplayer game platform), instead using XXX or OOO, this game use SOS, consist of 2 players.  
  
This game fully created by using ReactJS + Django REST + Django Framework.
There is another project that using ReactJS only, but does not support Scoring System.  
  
[SOS GAME REACJS ONLY HERE](https://gitlab.com/rd.pradipta/sos_game-react)

For now, it only support PC / Computer display, not yet supported for mobile display. This feature will come soon.  
This just my personal project to fullfil my hobby and to do something productive during winter holiday, so I am sorry if this app will not be as good as you expected.   
  
To run this app: You have to install Python, Django, Django REST, and NodeJS.  
Run:
1. npm run dev
2. python manage.py runserver

Created by: Pradipta Gitaya