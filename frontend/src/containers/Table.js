import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";

const Table = ({ data }) =>
	!data.length ? (
		<p>Belum ada Skor Tercatat!</p>
	) : (
		<div className="column">
			<h5 className="subtitle">
				Skor Semua Pengguna SOS
			</h5>
			<table className="table is-striped">
				<thead>
					<tr>
						<th>Nomor</th>
						<th>Nama</th>
						<th>Poin</th>
					</tr>
				</thead>
				<tbody>
					{data.map(el => (
						<tr key={el.id}>
							{Object.entries(el).map(el => <td key={key(el)}>{el[1]}</td>)}
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);

Table.propTypes = {
	data: PropTypes.array.isRequired
};

export default Table;