import React, { Component } from 'react';

import SOS from '../App';
import Game from './Game';
import Alert from '../components/Alert';
import Input from '../components/Input';
import Button from '../components/Button';
import image from '../assets/bg_1.png';

class Registration extends Component{
  constructor(props){
    super(props);
    this.state = {
      // Menyimpan nama dan status apakah boleh dilanjutkan
			idle : true,
			status : "",
			firstPlayer : "",
			secondPlayer : "",
			startGame : false,
		};
		this.handlePlayerName = this.handlePlayerName.bind(this);
	}

	// Handle input nama pemain secara general
  handlePlayerName = (e) => {
    let value = e.target.value; // Atribut value pada input
		let name = e.target.name; // Atribut name pada input
    this.setState({
			[name]:value
		});
	}
	
	// Memeriksa form untuk melanjutkan permainan
	checkName = () => {
		if(this.state.firstPlayer && this.state){
			if(this.state.firstPlayer.length >=3 && this.state.secondPlayer.length >= 3){
        if(this.state.firstPlayer.length <=10 && this.state.secondPlayer.length <= 10){
          if(this.state.firstPlayer !== this.state.secondPlayer)
            return true;
        }
			}
		}
		return false;
	}

	// Fungsi untuk memulai permainan dengan mengganti state
	startGame = () => {
		this.setState({
			startGame : true
		})
	}

	// Fungsi untuk melakukan render
  render(){
		let play_btn, alert_box, name_check, to_view = null;
		let empty_line = <br></br>
		let back_btn = 
			<Button onClick={() => {this.setState({idle : false}) }}
				type={"dark"}
				title={"KEMBALI"}
			/>;

		if(this.checkName() && !this.state.startGame){
			name_check = "Nama kedua pemain valid. Selamat Bermain!";
			play_btn =	
				<Button onClick={() => this.startGame()}
					type={"danger"}
					title={"MULAI BERMAIN"}
				/>;
			alert_box = <Alert type={"success"} content={name_check} style_addition={"text-center"}/>;
		}
		else{
			name_check = "Nama kedua pemain belum valid!"
			alert_box = <Alert type={"danger"} content={name_check} style_addition={"text-center"}/>;
		}
							
		if(this.state.startGame){
			return(
				<Game
					first_player={this.state.firstPlayer}
					second_player={this.state.secondPlayer}
				/>
			)
		}
	
		// Apabila dalam mode idle, maka tampilkan default
		if(this.state.idle){
			to_view =
				<div>
					<img className='bg' alt="background-img" src={image} />
					<div className="container w-50 object_centered">
						<div className="card shadow">
							<div className="card-body">
								<h5 className="text-center font-weight-bold"> Siapakah yang Akan Bermain </h5>
								{empty_line}
								<form className="container-fluid">
									<Input
										inputType={"text"}
										title={"Pemain 1"}
										name={"firstPlayer"}
										value={this.state.firstPlayer}
										placeholder={"Masukkan nama pemain 1"}
										handleChange={this.handlePlayerName}
									/>
									<Input
										inputType={"text"}
										title={"Pemain 2"}
										name={"secondPlayer"}
										value={this.state.secondPlayer}
										placeholder={"Masukkan nama pemain 2"}
										handleChange={this.handlePlayerName}
									/>
								</form>
								{empty_line}
								<div className="alert alert-dark" role="alert">
									<p> Silakan masukkan nama kedua pemain! Nama Pemain harus mengikuti aturan: </p>
									<ul>
										<li>Panjang Nama minimal 3 karakter & maksimal 10 karakter</li>
										<li>Semua pemain harus memiliki nama</li>
										<li>Nama antar pemain tidak boleh sama</li>
										<li>Nama tidak boleh mengandung unsur SARA dan bersifat menghina</li>
									</ul>
									<p> Tombol MULAI akan muncul apabila semua syarat terpenuhi </p>
								</div>
								{alert_box}
								<div className="row justify-content-around">
									<div className="col-4">
										{back_btn}
									</div>
									<div className="col-4">
										{play_btn}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		}
		else{ to_view = <SOS /> }
		return( to_view );
  }
} 

export default Registration