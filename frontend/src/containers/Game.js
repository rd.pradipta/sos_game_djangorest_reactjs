import React, { Component } from 'react';
import Board from './Board';
import image from '../assets/bg_1.png';
import Scoreboard from './Scoreboard';

class Game extends Component{
  constructor(props){
    super(props);
    this.state = {
      // Nanti bagian ini diisi menyimpan apakah sudah terisi atau belum
      idle : true,
      filled : Array(36).fill(null),
      firstPlayerTurn : true,
      firstPlayerPoint : 0,
      secondPlayerPoint : 0,
      totalTurn : 1,
    };
  }

  // Fungsi untuk menghandle apabila terjadi action click
  boardClickHandler = (i) => {
    const filled = this.state.filled.slice();
    // Cek pemenang, kalau sudah menang ATAU kotak sudah terisi, gabisa diganti - ganti
    if (this.checkWinner() != null || this.state.filled[i]){
      return ;
    }
    // Melakukan operasi lanjutan jika belum ada pemenang
    const currentLetter = this.state.totalTurn % 2 === 0 ? 'O' : 'S';
    filled[i] = currentLetter;
    // Lakukan cek SOS dan penambahan poin jika diperlukan
    const point_to_add = this.checkSOS(filled, currentLetter, i);
    // Mengubah state yang diperlukan saat click selesai
    this.setState({
      filled : filled,
      firstPlayerTurn : !this.state.firstPlayerTurn,
      totalTurn : this.state.totalTurn + 1,
      firstPlayerPoint : this.state.firstPlayerTurn ? this.state.firstPlayerPoint + point_to_add : this.state.firstPlayerPoint + 0,
      secondPlayerPoint : !this.state.firstPlayerTurn ? this.state.secondPlayerPoint + point_to_add : this.state.secondPlayerPoint + 0,
    });
  }

  // Fungsi untuk memeriksa SOS dan mengembalikan poin total yang harus ditambahkan
  checkSOS = (filled, curr_letter, pos) => {
    const up = pos-6;
    const down = pos+6;
    const left = pos-1;
    const right = pos+1;
    var point_to_add = 0;

    if(curr_letter === 'S'){
      // Cek ke arah atas, bawah, kiri, kanan
      point_to_add += this.validateWord(up,up-6,'O');
      point_to_add += this.validateWord(down,down+6,'O');
      point_to_add += this.validateWord(left,left-1,'O');
      point_to_add += this.validateWord(right,right+1,'O');
      // Cek ke arah atas, bawah, kiri, kanan secara diagonal
      point_to_add += this.validateWord(pos-5,pos-10,'O');
      point_to_add += this.validateWord(pos-7,pos-14,'O');
      point_to_add += this.validateWord(pos+5,pos+10,'O');
      point_to_add += this.validateWord(pos+7,pos+14,'O');
    }
    else{
      // Cek atas & bawah apakah 'S'
      point_to_add += this.validateWord(up,down,'S');
      point_to_add += this.validateWord(left,right,'S');
      // Cek secara diagonal
      point_to_add += this.validateWord(pos-5,pos+5,'S');
      point_to_add += this.validateWord(pos-7,pos+7,'S');

    }
    return point_to_add;
  }

  // Fungsi memvalidasi apakah terbentuk kata SOS ? Jika terbentuk, beri 10 poin
  validateWord = (to_check1, to_check2, checksum) => {
    if( to_check1 >= 0 && to_check2 >= 0){
      if(this.state.filled[to_check1] && this.state.filled[to_check2]){
        if(this.state.filled[to_check1] === checksum && this.state.filled[to_check2] === 'S'){
          return 10;
        }
      }
    }
    return 0;
  }

  checkWinner = () => {
    // Pemenang ditentukan jika board sudah terisi penuh
    var full = true;
    for(let i = 0; i < this.state.filled.length; i++){
      if(!this.state.filled[i]){
        full = false; // Menandakan bahwa ada yang kosong, karena null
        break;
      }
    }
    if(full){
      // Jika penuh, maka tentukan pemenang dengan membandingkan skor & menyimpan skor
      var conf = null;
      var name = this.props.first_player;
      var point = this.state.firstPlayerPoint;
      const player1_data = { name, point };
      conf = {
        method: "post",
        body: JSON.stringify(player1_data),
        headers: new Headers({ "Content-Type": "application/json" })
      };
      fetch("api/data/", conf).then(response => console.log(response));

      name = this.props.second_player;
      point = this.state.secondPlayerPoint;
      const player2_data = { name, point };
      conf = {
        method: "post",
        body: JSON.stringify(player2_data),
        headers: new Headers({ "Content-Type": "application/json" })
      };
      fetch("api/data/", conf).then(response => console.log(response));

      const word = " Menang! & Skor Kedua Pemain Telah Disimpan!";

      if (this.state.firstPlayerPoint > this.state.secondPlayerPoint)
        return this.props.first_player + word; // Player 1 Menang
      else if (this.state.firstPlayerPoint < this.state.secondPlayerPoint)
        return this.props.second_player + word; // Player 2 Menang
      else{
        return "Hasil Permainan Seri! Tidak Ada yang Menang!"; // Seri, tidak ada yang menang & tidak ada yang kalah
      }
    }
    // Belum ada pemenang
    return null;  
  }

  // Fungsi untuk melakukan reset game
  resetGame = () => {
    this.setState({
      filled : Array(36).fill(null),
      firstPlayerTurn : true,
      firstPlayerPoint : 0,
      secondPlayerPoint : 0,
      totalTurn : 1,
    });
  }

  render(){
    let to_view = null;
    const winner = this.checkWinner();
    var status;

    if (winner) status = 'Permainan telah Berakhir!\n' + winner;      
    else
      status = "Giliran Anda, " + (this.state.firstPlayerTurn ? this.props.first_player : this.props.second_player) + " !";
    
    if(this.state.idle){
      to_view =
      <div>
       <img className='bg' alt="background-img" src={image} />
       <div className="container object_centered">
          <div className="row">
            <div className="col-md-6">
              <div className="card shadow">
                <div className="card-body">                
                  <div className="game-board">
                    <Board 
                      filled={this.state.filled}
                      onClick={(i) => this.boardClickHandler(i)}
                    />
                  </div>
                </div>
              </div>
            </div>
            <Scoreboard
              message={status}
              first_player={this.props.first_player}
              second_player={this.props.second_player}
              first_point={this.state.firstPlayerPoint}
              second_point={this.state.secondPlayerPoint}
              reset={() => this.resetGame()}
            />
          </div>
        </div>
      </div>
    }
    else{

    }
    return ( to_view )
  }
}

export default Game