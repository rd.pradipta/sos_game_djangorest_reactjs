import React, { Component } from "react";
import PropTypes from "prop-types";

class DataProvider extends Component{
	static PropTypes = {
		endpoint: PropTypes.string.isRequired,
		render: PropTypes.func.isRequired
	};
	state = {
		data: [],
		loaded: false,
		placeholder: "Mohon Menunggu..."
	};
	componentDidMount(){
		fetch(this.props.endpoint)
			.then(response => {
				if(response.status !== 200){
					return this.setState({ placeholder: "Terjadi Kesalahan! Periksa Koneksi Internet Anda"});
				}
				return response.json();
			})
			.then(data => this.setState({ data: data, loaded: true}));
	}
	render(){
		const { data, loaded, placeholder} = this.state;
		return loaded ? this.props.render(data) : <p>{placeholder}</p>;
	}
}
export default DataProvider;