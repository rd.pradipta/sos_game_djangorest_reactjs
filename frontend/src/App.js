import React, { Component } from 'react';

import Registration from  './containers/Registration';
import DataProvider from "./containers/DataProvider";
import Table from "./containers/Table";

import Button from './components/Button';
import image from './assets/bg_1.png';

class SOS extends Component{
  constructor(props){
    super(props);
    this.state = {
			idle : true,
			showHelp : false,
			showScores : false,
		};
	}

	// Fungsi untuk memanggil komponen registration
	startRegistration = () => {
		this.setState({
			idle : false
		})
	}

	// Fungsi untuk mengubah state untuk menampilkan about
	showScores = () => {
		this.setState({
			showScores : !this.state.showScores,
			showHelp : false
		})
	}

	// Fungsi untuk mengubah state untuk menampilkan bantuan
  showHelp = () => {
		this.setState({
			showHelp : !this.state.showHelp,
			showScores : false
		})
	}

	render(){
		let help_panel, score_panel, to_view = null;
		let empty_line = <br></br>

		if(this.state.showScores){		
			score_panel = 
			<React.Fragment>
				<DataProvider endpoint="api/data/" 
					render={ data => <Table data={data} />} 
				/>
			</React.Fragment>
	
		}
		if(this.state.showHelp){
			help_panel = 
        <div className="text-left">
					<h5 className="font-weight-bold"> Apa itu SOS Game </h5>
					<p> Permainan sederhana berbasis web dengan menggunakan ReactJS. 
							SOS terinspirasi dari salah satu game di Hago, dan pembuat aplikasi ini
							mencoba membuat versi sederhana dengan berbasis web
					</p>
				  <h5 className="font-weight-bold"> Bagaimana Cara Memainkan SOS </h5>
          <p> Permainan ini terdiri dari 2 pemain, SOS mirip sekali dengan Tic Tac Toe,
							hanya saja Anda tidak diminta untuk membentuk XXX maupun OOO, melainkan SOS.
              Pemain 1 diberikan huruf S, sedangkan pemain 2 diberikan huruf O. Pemain dapat
							membentuk SOS secara vertikal, horizontal, bahkan diagonal
							Setiap SOS yang terbentuk bernilai 10 poin. Pemenangnya adalah pemain yang
							mendapatkan poin terbanyak jika permainan telah berakhir. Permainan berakhir
							apabila kotak 6x6 telah terisi seluruhnya.
          </p>
				  <h5 className="font-weight-bold"> Apakah Skor Bisa Disimpan </h5>
          <p> Fitur penyimpanan skor akan tersedia pada masa depan </p>
				</div>
		}
		if(this.state.idle){
			to_view = 
			<div>
       <img className='bg' alt="background-img" src={image} />
				<div className="container w-50 object_centered">
					<div className="card shadow">
						<div className="card-body text-center">
							<h1> SOS GAME </h1>
							<blockquote className="blockquote">
								<p>Saatnya kamu meminta bantuan sebanyak mungkin!</p>
							</blockquote>
							{empty_line}
							<div className="row">
								<div className="col">
									<Button 
										onClick={() => this.showHelp()} 
										type={"dark"}
										title={"Bantuan SOS"}
									/>
								</div>
								<div className="col">
									<Button
										onClick={() => this.startRegistration()} 
										type={"danger"}
										title={"MAIN SEKARANG"}
									/>
								</div>
								<div className="col">
									<Button
										onClick={() => this.showScores()} 
										type={"dark"}
										title={"Semua Peringkat"}
									/>
								</div>
							</div>
							{empty_line}
							{help_panel}
							{score_panel}
						</div>
					</div>
				</div>
			</div>
		}
		else{	to_view = <Registration /> }
		return( to_view )
	}
}

export default SOS